from django.apps import AppConfig


class DjangocmsBlogViewRestrictionsConfig(AppConfig):
    name = "djangocms_blog_view_restrictions"
