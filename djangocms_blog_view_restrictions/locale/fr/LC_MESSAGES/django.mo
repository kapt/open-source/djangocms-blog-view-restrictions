��          \      �       �   *   �      �        $   "     G     Y     _  >  d  U   �  3   �     -  .   @     o     �     �                                       Please select user or group, but not both. Please select user or group. View restriction View restriction for post #{post_id} View restrictions group user Project-Id-Version: 0.1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Adrien Delhorme <ad@kapt.mobi>
Language-Team: Kapt <dev@kapt.mobi>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Veuillez sélectionner un utilisateur ou un groupe, mais pas les deux en même temps. Veuillez sélectionner un utilisateur ou un groupe. Restriction de vue Restriction de vue pour l’article #{post_id} Restrictions de vue groupe utilisateur 